/*********************************************************************************************
* Copyright (c) 2011                                                                         *
*                                                                                            *
* Allan Feldman <allan.feldman(__AT__)gatech.edu>                                            *
* Steven George <s.george(__AT__)gatech.edu>                                                 *
*                                                                                            *
* Permission is hereby granted, free of charge, to any person obtaining                      *
* a copy of this software and associated documentation files (the "Software"),               *
* to deal in the Software without restriction, including without limitation the              *
* rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           *
* copies of the Software, and to permit persons to whom the Software is furnished            *
* to do so, subject to the following conditions:                                             *
*                                                                                            *
* The above copyright notice and this permission notice shall be included in all copies      *
* or substantial portions of the Software.                                                   *
*                                                                                            *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        *
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  *
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       *
* OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     *
* DEALINGS IN THE SOFTWARE.                                                                  *
*********************************************************************************************/

#include <iostream>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <set>
#include <string>
#include <fstream>
#include <sstream>
#include <syscall.h> // NR_write, etc
#include <stdio.h>
#include <sys/reg.h> // ORIG_EAX
#include <sys/user.h> // user_regs_struct

#define NAME "ftpmon"
#define ACL_FILE "gtftpmon.acl"
#define ACL_NEGATIVE_PERM 4
#define ACL_READ_PERM 1
#define ACL_WRITE_PERM 2
#define SYSCALLENTRY -38
#define READ_REQUEST 34816
#define WRITE_REQUEST 36417
#define __SYS_WAIT4 114
#define __SYS_EXIT_GROUP 252

/* Struct for ACL permissions */
struct acl_element_t
{
    unsigned int id; // group/user id
    bool isGroupId; // true if ID is a group ID, false if user ID
    bool isNegative; // true if permission is a negative permission
    bool canRead;   // permission to read?
    bool canWrite;  // permission to write?
};

using namespace std;

typedef std::map<std::string, std::vector<acl_element_t> > acl_t;

void traceThisPID(int pidParam);
void attachToProcess(int pidParam);
void siginthandler(int sig);
void getdata(pid_t child, long addr, char *str, int len);
bool parseAclFromFile(const char *filename, acl_t *acl);
bool isRequestAllowed(unsigned int uid, string &filename, bool isRequestRead, acl_t &acl);
bool getGroups(unsigned int uid, set<unsigned int>& gid);
std::string exec(const char cmd[]);

const int long_size = sizeof(long);
string currentDir = "/";

int main(int argc, char* argv[])
{
    /* check for pid from console */
    if(argc != 2)
    {
        cout << "Invalid syntax. Usage: sudo ./"NAME" [pid]" << endl;
        exit(1);
    }

    /* read in pid from console */
    int topLvlProcessPID = atoi(argv[1]);
    
    /* handle CTRL-C signal to PTRACE_DETACH from all vsftpd processes */
    signal(SIGINT, siginthandler);

    traceThisPID(topLvlProcessPID);

    return 0;
}

void traceThisPID(int pidParam)
{
    int tracedProgPID = pidParam;

    /* hook onto process and become its parent */
    attachToProcess(tracedProgPID);

    int status;
    struct user_regs_struct regs;   
    
    /* Declare a variable containing the access control list */
    acl_t acl;
    
    /* read and parse ACL file */
    if (!parseAclFromFile(ACL_FILE, &acl))
    {
        cout << "[ERROR: Failed to parse ACL " << ACL_FILE << "]" << endl;
        exit(1);
    }

    while(1)
    {
        /* wait here until traced program does system call and pauses */
        wait(&status);
        
        /* gather details about system call and process that called it */
        ptrace(PTRACE_GETREGS, tracedProgPID, NULL, &regs);
        siginfo_t signalinfo;
        ptrace(PTRACE_GETSIGINFO, tracedProgPID, NULL, &signalinfo);

        /* if traced process no longer exists, end tracing */
        if(WIFEXITED(status) && tracedProgPID!=pidParam)
        {           
            cout << "PTRACE_DETACH -> " << tracedProgPID << endl;
            ptrace(PTRACE_DETACH, tracedProgPID, NULL, NULL);           
            if(signalinfo.si_uid == 65534) abort();
            break;
        }

        /* system call number in orig_eax */
        if(regs.orig_eax == __NR_clone || regs.orig_eax == __NR_open
           || regs.orig_eax == __NR_getcwd || regs.orig_eax == __NR_chdir)
        {
            if(regs.orig_eax == __NR_open)
            {
                if(signalinfo.si_uid !=0) // if not root
                {
                    if(regs.eax == SYSCALLENTRY) 
                    {
                        /* get the requested filename from ebx register */
                        cout << "[pid " << tracedProgPID << "] open(";
                        char fname[50];                     
                        getdata(tracedProgPID, regs.ebx, fname, 49);        
                        cout << fname << ")" << endl;           
                        
                        string fnameString(fname);
                        
                        /* append current path if full path not included */
                        if (fnameString.at(0)!='/')
                        {
                            fnameString=currentDir + fnameString;
                        }
                        
                        /* Check to see if user is allowed to make request */
                        if (!isRequestAllowed(signalinfo.si_uid, fnameString, regs.ecx==READ_REQUEST, acl))
                        {
                            /* Debug Info - Read/Write request */
                            cout << "[Info] Requested File ";
                            if (regs.ecx==READ_REQUEST)
                            {
                                cout << "Read";
                            }
                            else
                            {
                                cout << "Write";
                            }
                            cout << ": " << fnameString << " ACCESS DENIED" << endl;
                            
                            /* Deny Request by Cancelling SYSCALL */
                            regs.orig_eax=-1;
                            ptrace(PTRACE_SETREGS, tracedProgPID, NULL, &regs);
                        }
                        else
                        {
                            /* Debug Info - Read/Write request */
                            cout << "[Info] Requested File ";
                            if (regs.ecx==READ_REQUEST)
                            {
                                cout << "Read";
                            }
                            else
                            {
                                cout << "Write";
                            }
                            cout << ": " << fnameString << " ACCESS GRANTED" << endl;
                        }

                    }   
                }      
            }           
            else if(regs.orig_eax == __NR_getcwd && regs.eax != SYSCALLENTRY && signalinfo.si_uid !=0)
            {
                char dname[50];
                getdata(tracedProgPID, regs.ebx, dname, 49);
                string dnameString(dname);
                currentDir = dnameString + (dnameString.at(dnameString.length()-1)=='/'?"":"/");
            }
            else if(regs.orig_eax == __NR_clone)
            {
                if (regs.eax > 100) // if PID is valid and not at sys call entry
                {
                    /* attach to PID of tracee clone in regs.eax */
                    pid_t newTracer = fork();
                    if(newTracer == 0)
                    {
                        attachToProcess(regs.eax);
                        tracedProgPID = regs.eax;
                    }
                }
            }
        }

        /* resume execution of traced program until next system call */
        ptrace(PTRACE_SYSCALL, tracedProgPID, NULL, NULL);

        usleep(1);
    }
}

void attachToProcess(int pidParam)
{
    if (pidParam < 0 || ptrace(PTRACE_ATTACH, pidParam, NULL, NULL) == -1L)
    {
        cout << "[ERROR: Failed to attach to process with PID " << pidParam << "]" << endl;
        exit(1); // no statement after this will be executed
    }
    else
    {
        cout << "PTRACE_ATTACH ->  " << pidParam << endl;
    }
}

void siginthandler(int sig)
{
    exit(1);
}

/* source: http://www.linuxjournal.com/node/6210/print */
void getdata(pid_t child, long addr,
             char *str, int len)
{   
    char *laddr;
    int i, j;
    union u 
    {
        long val;
        char chars[long_size];
    }data;
    i = 0;
    j = len / long_size;
    laddr = str;
    while(i < j) 
    {
        data.val = ptrace(PTRACE_PEEKDATA, child,
                  addr + i * 4, NULL);
        memcpy(laddr, data.chars, long_size);
        ++i;
        laddr += long_size;
    }
    j = len % long_size;
    if(j != 0)
    {
        data.val = ptrace(PTRACE_PEEKDATA, child,
                  addr + i * 4, NULL);
        memcpy(laddr, data.chars, j);
    }
    str[len] = '\0';
}

bool parseAclFromFile(const char *filename, acl_t *acl)
{
    ifstream input(filename);
    unsigned int lineNumber=1;
    for (string entry; getline(input, entry, '\n'); ++lineNumber )
    {
        stringstream entryStream(entry);
        
        /* Get target file the entry row is describing */
        string target;
        std::getline(entryStream, target, ' ');
        
        /* For each ACL entry on a given file (will skip blank lines) */
        for(string id ; std::getline(entryStream, id, ' '); )
        {
            string permission;
            getline(entryStream, permission, ' ');
            /* Check if there is a missing field */
            if (entryStream.fail())
            {
                cout << "[ERROR(ACL Line:"<<lineNumber<<"): missing information (id or permission)]" <<endl;
                return false;
            }
            
            acl_element_t aclElement;
            
            /* Check if ID is malformed */
            if (id[0]!='g' && id[0]!='u' || id.length()<2 || id[1]>='9' || id[1]<='0')
            {
                cout << "[ERROR(ACL Line:" << lineNumber <<"): Malformed ID:" << id << "]"<<endl;
                return false;
            }
            /* Parse ID information */
            aclElement.isGroupId = id[0]=='g';
            id.erase(0,1); // delete g or u identifier to get the pure id
            aclElement.id = atoi(id.c_str());
            
            /* Parse permission information*/
            unsigned int permissionNumber = atoi(permission.c_str());
            aclElement.isNegative = permissionNumber & ACL_NEGATIVE_PERM;
            aclElement.canRead = permissionNumber & ACL_READ_PERM;
            aclElement.canWrite = permissionNumber & ACL_WRITE_PERM;
            
            /* Add parsed ACL element to ACL */
            (*acl)[target].push_back(aclElement);
        }
    }
    return true;
}

bool isRequestAllowed(unsigned int uid, string &filename, bool isRequestRead, acl_t &acl)
{
    /* always allow root listing */
    if (filename=="/.") { return true; }
    
    static unsigned int oldUid = 0; // root is always allowed
    static set<unsigned int> gid;
    if (oldUid!=uid)
    {
        oldUid=uid;
        if (!getGroups(uid,gid))
        {
            cout << "[ERROR] Unable to get groups for UID: " << uid << endl;
            exit(1);
        }
    }
    
    bool aclIndicatesAllowed=false; // acl can indicate allowed but a negative permission will override this
    std::vector<acl_element_t>::iterator iter;
    
    /* For initial directory and all directories on top of it (do-while: all files have at least 1 dir) */
    string requestedFileDir = filename;
    string directoryKey;
    do
    {
        /* Get directory on top of requestedFileDir */
        requestedFileDir = requestedFileDir.substr(0,requestedFileDir.rfind("/"));
        /* All keys contain a "/" at the end for directories */
        directoryKey = requestedFileDir + "/";
        
        /* if directory permissions exist */
        if (acl.count(directoryKey)!=0)
        {
            for (iter=acl[directoryKey].begin(); iter!=acl[directoryKey].end(); ++iter)
            {
                /* if permission is relevant to one of the user's GIDs or their UID */
                if (((*iter).isGroupId && gid.count((*iter).id)>0) || (!((*iter).isGroupId) && (*iter).id==uid))
                {
                    /* if positive permission is given for directory access */
                    if (!(*iter).isNegative)
                    {
                        /* Allow Directory Listings of approved directories */
                        if ((filename.at(filename.length()-1)=='.'))
                        {
                            return true;
                        }
                    }
                    /* if negative permission says to deny */
                    else if ((*iter).isNegative && !(isRequestRead?(*iter).canRead:(*iter).canWrite))
                    {
                        return false;
                    }
                }
            }
        }
    } while (directoryKey!="/");
    
    /* if ACL permissions exist for that filename */
    if (acl.count(filename)!=0)
    {
        for (iter=acl[filename].begin(); iter!=acl[filename].end(); ++iter)
        {
            /* if permission is relevant to one of the user's GIDs or their UID */
            if (((*iter).isGroupId && gid.count((*iter).id)>0) || (!((*iter).isGroupId) && (*iter).id==uid))
            {
                /* if positive permission is given for file access */
                if (!(*iter).isNegative && (isRequestRead?(*iter).canRead:(*iter).canWrite))
                {
                    /* ACL then indicates that permission is to be granted if not later denied */
                    aclIndicatesAllowed=true;
                }
                /* if negative permission says to deny */
                else if ((*iter).isNegative && !(isRequestRead?(*iter).canRead:(*iter).canWrite))
                {
                    return false;
                }
            }
        }
    }
    
    return aclIndicatesAllowed;
}

bool getGroups(unsigned int uid, set<unsigned int>& gid)
{
    /* variable declaration */
    stringstream command;
    string result;
    unsigned int substrPos;
    
    /* run command to convert uid to string username */
    command << "cat /etc/passwd | grep '[a-zA-Z]:[a-zA-Z]:" << uid << "'";
    result=exec(command.str().c_str());
    if (result.empty())
    {
        cout << "[ERROR] unable to exec command: " << command.str() << endl;
        return false;
    }
    
    substrPos = result.find(':');
    result = result.substr(0,substrPos);
    
    /* flush out command stream */
    command.str("");
    
    /* get groups associated with user name string */
    command << "id -G " << result;
    result=exec(command.str().c_str());
    if (result.empty())
    {
        cout << "[ERROR] unable to exec command: " << command.str() << endl;
        return false;
    }
    
    /* generate group id set */
    stringstream groupStream(result);
    for (string group; getline(groupStream, group, ' '); )
    {
        gid.insert(atoi(group.c_str()));
    }
    
    return true;
}

/* source: http://stackoverflow.com/questions/478898/how-to-execute-a-command-and-get-output-of-command-within-c */
std::string exec(const char cmd[]) {
    FILE* pipe = popen(cmd, "r"); // see project_report.pdf for justification
    if (!pipe) {
        cout << "[ERROR] unable to exec command: " << cmd << endl;
        exit(1);
    }
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
                result += buffer;
    }
    pclose(pipe);
    return result;
}
