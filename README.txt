TO USE:
0) *must* use 32-bit Ubuntu Linux 8.04.4 (see project_report.pdf for details)
1) get PID of vsftpd daemon: ps aux | grep vsftpd
2) sudo ./ftpmon PID
3) in another terminal : ftp 127.0.0.1
4) to trigger open system call for read, in ftp terminal: get [remote file name] [local file name]
5) to test permissions, do not include paths for any file (local file will show up in user's home)

ACL FORMAT:
<file/directory> <u/g><id> <permission#> <u/g><id> <permission#> ...

see example acl "gtftpmon.acl"
*** NOTE: Full paths must be included using root as ftp directory (/x refers to file x in /home/ftp) ***
placing u at the front of an id indicates it is a uid
placing g at the front of an id indicates it is a gid

PERMISSION NUMBERS:
0 - positive: grant nothing
1 - positive: grant read
2 - positive: grant write
3 - positive: grant read/write
4 - negative: deny read/write
5 - negative: deny write
6 - negative: deny read
7 - negative: deny nothing